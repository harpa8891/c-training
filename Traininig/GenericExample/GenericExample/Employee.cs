﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class Employee
    {
        int id;
        string name;
        protected double salary;
        static int count;
        static string companyName;
        

        //Static Constructor
        static Employee()
        {
            Console.WriteLine("static");
            count = 100;
            companyName = "Tricentis India";
        }

        //Default Constrctor
        public Employee()
        {
            Console.WriteLine("Defualt");
            count++;
            this.id = 102;
            this.name = "Nilesh";
            this.salary = 12345;
            
        }

        //Parameterized Constructor
        public Employee(int id, string name, double sal)
        {
            Console.WriteLine("parameterized");
            count++;
            this.id = id;
            this.name = name;
            this.salary = sal;
        }

        public Employee(string name,int id, double sal)
        {
            count++;
            Console.WriteLine("parameterized");
            this.id = id;
            this.name = name;
            this.salary = sal;
        }

        //Copy Contructor

        public Employee(Employee e)
        {
            count++;
            Console.WriteLine("Copy");
            this.id = e.id;
            this.name = e.name;
            this.salary = e.salary;
        }

        public void SetEmpInfo(int id,string name,double sal)
        {
            this.id = id;
            this.name = name;
            this.salary = sal;
        }
        public virtual string GetEmpInfo()
        {
            return "Emp Info: "+this.id + " " + this.name + " " + this.salary+" "+companyName;
        }
        public static int GetCount()
        {
            return count;
        }
        public static string GetCompanyName()
        {
            return companyName;
        }
        public static void SetCompanyName(string cn)
        {
            companyName=cn;
        }
        public virtual double CalSal()
        {
            return this.salary;
        }

    }
}
