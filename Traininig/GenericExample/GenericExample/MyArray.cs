﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericExample
{
    class MyArray<T>
    {
        public T[] myarr;

        public MyArray(int size)
        {
            myarr = new T[size];
        }
        public T GetItem(int index)
        {
            return myarr[index];
        }
        public void SetItem(int index,T val)
        {
            myarr[index] = val;
        }
        public void DisplayAll()
        {
            foreach(T type in myarr)
            {
                Console.WriteLine(type);
            }
        }
    }
}
