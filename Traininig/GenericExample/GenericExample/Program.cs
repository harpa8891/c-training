﻿using OfficeMangement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericExample
{
    class Program
    {
        public static void Swap<T>(ref T val1, ref T val2)
        {
            T tmp = val1;
            val1 = val2;
            val2 = tmp;
        }
        static void Main(string[] args)
        {
            
            int x = 5;
            int y = 7;
            
            char c1 = 'a';
            char c2 = 'r';
            Console.WriteLine("Before swapping x={0}, y={1}, c1={2}, c2={3}", x, y, c1, c2);
            Swap(ref x, ref y);
            Swap(ref c1, ref c2);
            Console.WriteLine("After swapping x={0}, y={1}, c1={2}, c2={3}",x,y,c1,c2);
            Console.ReadLine();
        }
         
        
            static void Main1(string[] args)
        {
            MyArray<int> numbers = new MyArray<int>(5);
            numbers.SetItem(0, 1);
            numbers.SetItem(1, 2);
            numbers.SetItem(2, 3);
            numbers.SetItem(3, 4);
            numbers.SetItem(4, 5);

            if(numbers.GetItem(2)==3)
            {
                numbers.SetItem(2, 33);
            }
            numbers.DisplayAll();

            //For String.
            MyArray<string> names = new MyArray<string>(5);
            names.SetItem(0, "aaa");
            names.SetItem(1, "bbb");
            names.SetItem(2,"ccc");
            names.SetItem(3, "ddd");
            names.SetItem(4, "eee");

            if (names.GetItem(2) == "ccc")
            {
                names.SetItem(2, names.GetItem(2).ToUpper());
            }
            names.DisplayAll();

            MyArray<Employee> empArr = new MyArray<Employee>(2);
            empArr.SetItem(0, new Engg(101, "harshad", 12345, 50, 145));
            empArr.SetItem(1, new Sales(102, "Nilesh", 14235, 1452, 14250));

            foreach(Employee emp in empArr.myarr)
            {
                Console.WriteLine(emp.GetEmpInfo());
            }
            List<MyArray<int>> list = new List<MyArray<int>>();
            list.Add(new MyArray<int>(3));


            Console.ReadLine();
        }
    }
}
