﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("App Start!!!!");
            Thread t1 = new Thread(Fun1);
            Thread t2 = new Thread(Fun2);
            Thread t3 = new Thread(() => Fun3(333333));
            t1.IsBackground = true;
            t2.IsBackground = true;
            t3.IsBackground = true;
            Console.WriteLine("thread Start");
            t1.Start();
            t2.Start();
            t3.Start();
            Console.WriteLine("App End!!!!");
        }
        static void Fun1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("1111111111111111111........");
                Thread.Sleep(300);
            }
        }
        static void Fun2()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("22222222222222222222222........");
                Thread.Sleep(300);
            }
        }
        static void Fun3(int val)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(val+"........");
                Thread.Sleep(300);
            }
        }

    }
}
