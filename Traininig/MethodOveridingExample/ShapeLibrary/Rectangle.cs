﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
   public class Rectangle:Shape
    {
        public double length { get; set; }
        public double width { get; set; }
        public Rectangle(string nm, double len, double wid):base(nm)
        {
            this.length = len;
            this.width = wid;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo()+" "+this.length+" "+this.width;
        }
        public override double CalArea()
        {
            return (this.length*this.width);
        }
    }
}
