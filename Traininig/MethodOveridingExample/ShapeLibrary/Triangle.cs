﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
   public class Triangle:Shape
    {
        public double height { get; set; }

        public double Base { get; set; }

        public Triangle(string nm, double h,double b):base(nm)
        {
            this.height = h;
            this.Base = b;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo()+" "+this.height+" "+this.Base;
        }
        public override double CalArea()
        {
            return (0.5*this.Base*this.height);
        }
    }
}
