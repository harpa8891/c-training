﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplicationExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Cutomer[] customers = new Cutomer[2];
            customers[0] = new FD("Harshad", 10145782369, 45128, 5);
            customers[1] = new LoanCustomer("Nilesh", 14578963251, 789654,8, 7.9);

            foreach(Cutomer c in customers)
            {
                Console.WriteLine(c.GetInfo());
                Console.WriteLine("Balance: "+c.CalBalance());
            }
        }
    }
}
