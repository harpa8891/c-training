﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplicationExample
{
    class Cutomer
    {
        public string name { get; set; }

        public double accountNumber { get; set; }

        public double balance { get; set; }      
        public Cutomer(string nm,double acn,double bal)
        {
            this.name = nm;
            this.accountNumber = acn;
            this.balance = bal;
            
        }
        public virtual string GetInfo()
        {
            return "Customer Info: " + name + " " + accountNumber + " " + balance ;
        }
        public virtual double CalBalance()
        {
            return balance;
        }

        
    }
}
