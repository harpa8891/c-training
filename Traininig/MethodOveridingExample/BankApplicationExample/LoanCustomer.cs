﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplicationExample
{
    class LoanCustomer : Cutomer
    {
        

        public int year { get; set; }

        public double intrestRate { get; set; }

        public LoanCustomer(string nm, double acn, double bal,int year,double intrest) : base(nm, acn, bal)
        {
            this.year = year;
            this.intrestRate = intrest;
        }
        public override string GetInfo()
        {
            return base.GetInfo()+" "+this.year+" "+this.intrestRate;
        }
        public override double CalBalance()
        {
            return base.CalBalance()*(intrestRate*year);
        }

    }
}
