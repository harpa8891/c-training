﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApplicationExample
{
    class FD:Cutomer
    {
        public int year { get; set; }
        public FD(string nm, double acn, double bal,int year) : base(nm, acn, bal)
        {
            this.year = year;
        }
        public override string GetInfo()
        {
            return base.GetInfo()+" "+year;
        }
        public override double CalBalance()
        {
            return base.CalBalance()*year;
        }

    }
}
