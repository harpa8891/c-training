﻿namespace WinFormShapes
{
    partial class Shapes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_shape = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.labl_TypesofShape = new System.Windows.Forms.Label();
            this.cmb_typeofshapes = new System.Windows.Forms.ComboBox();
            this.gr_squre = new System.Windows.Forms.GroupBox();
            this.txt_side = new System.Windows.Forms.TextBox();
            this.lbl_side = new System.Windows.Forms.Label();
            this.gr_cir = new System.Windows.Forms.GroupBox();
            this.txt_rad = new System.Windows.Forms.TextBox();
            this.lbl_rad = new System.Windows.Forms.Label();
            this.gr_rec = new System.Windows.Forms.GroupBox();
            this.txt_bre = new System.Windows.Forms.TextBox();
            this.lbl_bre = new System.Windows.Forms.Label();
            this.txt_wid = new System.Windows.Forms.TextBox();
            this.lbl_width = new System.Windows.Forms.Label();
            this.gr_triangle = new System.Windows.Forms.GroupBox();
            this.txt_high = new System.Windows.Forms.TextBox();
            this.lbl_high = new System.Windows.Forms.Label();
            this.txt_base = new System.Windows.Forms.TextBox();
            this.lbl_base = new System.Windows.Forms.Label();
            this.btn_AddShape = new System.Windows.Forms.Button();
            this.btn_Showall = new System.Windows.Forms.Button();
            this.btn_Count = new System.Windows.Forms.Button();
            this.Collection_of_shapes = new System.Windows.Forms.ListBox();
            this.gr_squre.SuspendLayout();
            this.gr_cir.SuspendLayout();
            this.gr_rec.SuspendLayout();
            this.gr_triangle.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_shape
            // 
            this.lbl_shape.AutoSize = true;
            this.lbl_shape.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_shape.Location = new System.Drawing.Point(85, 23);
            this.lbl_shape.Name = "lbl_shape";
            this.lbl_shape.Size = new System.Drawing.Size(45, 16);
            this.lbl_shape.TabIndex = 0;
            this.lbl_shape.Text = "Name";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(172, 23);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(121, 20);
            this.txt_name.TabIndex = 1;
            // 
            // labl_TypesofShape
            // 
            this.labl_TypesofShape.AutoSize = true;
            this.labl_TypesofShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labl_TypesofShape.Location = new System.Drawing.Point(46, 73);
            this.labl_TypesofShape.Name = "labl_TypesofShape";
            this.labl_TypesofShape.Size = new System.Drawing.Size(111, 16);
            this.labl_TypesofShape.TabIndex = 7;
            this.labl_TypesofShape.Text = "Types of Shapes";
            // 
            // cmb_typeofshapes
            // 
            this.cmb_typeofshapes.FormattingEnabled = true;
            this.cmb_typeofshapes.Items.AddRange(new object[] {
            "Traingle",
            "Rectangle",
            "Circle",
            "Square"});
            this.cmb_typeofshapes.Location = new System.Drawing.Point(172, 73);
            this.cmb_typeofshapes.Name = "cmb_typeofshapes";
            this.cmb_typeofshapes.Size = new System.Drawing.Size(121, 21);
            this.cmb_typeofshapes.TabIndex = 8;
            this.cmb_typeofshapes.Text = "Select One";
            this.cmb_typeofshapes.SelectedIndexChanged += new System.EventHandler(this.cmb_typeofshapes_SelectedIndexChanged);
            // 
            // gr_squre
            // 
            this.gr_squre.Controls.Add(this.txt_side);
            this.gr_squre.Controls.Add(this.lbl_side);
            this.gr_squre.Location = new System.Drawing.Point(31, 163);
            this.gr_squre.Name = "gr_squre";
            this.gr_squre.Size = new System.Drawing.Size(219, 111);
            this.gr_squre.TabIndex = 9;
            this.gr_squre.TabStop = false;
            this.gr_squre.Text = "Square";
            // 
            // txt_side
            // 
            this.txt_side.Location = new System.Drawing.Point(90, 32);
            this.txt_side.Name = "txt_side";
            this.txt_side.Size = new System.Drawing.Size(100, 20);
            this.txt_side.TabIndex = 9;
            // 
            // lbl_side
            // 
            this.lbl_side.AutoSize = true;
            this.lbl_side.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_side.Location = new System.Drawing.Point(17, 32);
            this.lbl_side.Name = "lbl_side";
            this.lbl_side.Size = new System.Drawing.Size(36, 16);
            this.lbl_side.TabIndex = 3;
            this.lbl_side.Text = "Side";
            // 
            // gr_cir
            // 
            this.gr_cir.Controls.Add(this.txt_rad);
            this.gr_cir.Controls.Add(this.lbl_rad);
            this.gr_cir.Location = new System.Drawing.Point(311, 163);
            this.gr_cir.Name = "gr_cir";
            this.gr_cir.Size = new System.Drawing.Size(219, 111);
            this.gr_cir.TabIndex = 10;
            this.gr_cir.TabStop = false;
            this.gr_cir.Text = "Circle";
            // 
            // txt_rad
            // 
            this.txt_rad.Location = new System.Drawing.Point(90, 32);
            this.txt_rad.Name = "txt_rad";
            this.txt_rad.Size = new System.Drawing.Size(100, 20);
            this.txt_rad.TabIndex = 9;
            // 
            // lbl_rad
            // 
            this.lbl_rad.AutoSize = true;
            this.lbl_rad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_rad.Location = new System.Drawing.Point(17, 32);
            this.lbl_rad.Name = "lbl_rad";
            this.lbl_rad.Size = new System.Drawing.Size(51, 16);
            this.lbl_rad.TabIndex = 3;
            this.lbl_rad.Text = "Radius";
            // 
            // gr_rec
            // 
            this.gr_rec.Controls.Add(this.txt_bre);
            this.gr_rec.Controls.Add(this.lbl_bre);
            this.gr_rec.Controls.Add(this.txt_wid);
            this.gr_rec.Controls.Add(this.lbl_width);
            this.gr_rec.Location = new System.Drawing.Point(581, 163);
            this.gr_rec.Name = "gr_rec";
            this.gr_rec.Size = new System.Drawing.Size(219, 111);
            this.gr_rec.TabIndex = 11;
            this.gr_rec.TabStop = false;
            this.gr_rec.Text = "Rectangle";
            // 
            // txt_bre
            // 
            this.txt_bre.Location = new System.Drawing.Point(90, 76);
            this.txt_bre.Name = "txt_bre";
            this.txt_bre.Size = new System.Drawing.Size(100, 20);
            this.txt_bre.TabIndex = 11;
            // 
            // lbl_bre
            // 
            this.lbl_bre.AutoSize = true;
            this.lbl_bre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bre.Location = new System.Drawing.Point(17, 76);
            this.lbl_bre.Name = "lbl_bre";
            this.lbl_bre.Size = new System.Drawing.Size(55, 16);
            this.lbl_bre.TabIndex = 10;
            this.lbl_bre.Text = "Breadth";
            // 
            // txt_wid
            // 
            this.txt_wid.Location = new System.Drawing.Point(90, 32);
            this.txt_wid.Name = "txt_wid";
            this.txt_wid.Size = new System.Drawing.Size(100, 20);
            this.txt_wid.TabIndex = 9;
            // 
            // lbl_width
            // 
            this.lbl_width.AutoSize = true;
            this.lbl_width.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_width.Location = new System.Drawing.Point(17, 32);
            this.lbl_width.Name = "lbl_width";
            this.lbl_width.Size = new System.Drawing.Size(42, 16);
            this.lbl_width.TabIndex = 3;
            this.lbl_width.Text = "Width";
            // 
            // gr_triangle
            // 
            this.gr_triangle.Controls.Add(this.txt_high);
            this.gr_triangle.Controls.Add(this.lbl_high);
            this.gr_triangle.Controls.Add(this.txt_base);
            this.gr_triangle.Controls.Add(this.lbl_base);
            this.gr_triangle.Location = new System.Drawing.Point(856, 163);
            this.gr_triangle.Name = "gr_triangle";
            this.gr_triangle.Size = new System.Drawing.Size(219, 111);
            this.gr_triangle.TabIndex = 12;
            this.gr_triangle.TabStop = false;
            this.gr_triangle.Text = "Triangle";
            // 
            // txt_high
            // 
            this.txt_high.Location = new System.Drawing.Point(90, 72);
            this.txt_high.Name = "txt_high";
            this.txt_high.Size = new System.Drawing.Size(100, 20);
            this.txt_high.TabIndex = 12;
            // 
            // lbl_high
            // 
            this.lbl_high.AutoSize = true;
            this.lbl_high.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_high.Location = new System.Drawing.Point(17, 76);
            this.lbl_high.Name = "lbl_high";
            this.lbl_high.Size = new System.Drawing.Size(47, 16);
            this.lbl_high.TabIndex = 11;
            this.lbl_high.Text = "Height";
            // 
            // txt_base
            // 
            this.txt_base.Location = new System.Drawing.Point(90, 32);
            this.txt_base.Name = "txt_base";
            this.txt_base.Size = new System.Drawing.Size(100, 20);
            this.txt_base.TabIndex = 9;
            // 
            // lbl_base
            // 
            this.lbl_base.AutoSize = true;
            this.lbl_base.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_base.Location = new System.Drawing.Point(17, 32);
            this.lbl_base.Name = "lbl_base";
            this.lbl_base.Size = new System.Drawing.Size(40, 16);
            this.lbl_base.TabIndex = 3;
            this.lbl_base.Text = "Base";
            // 
            // btn_AddShape
            // 
            this.btn_AddShape.Location = new System.Drawing.Point(207, 330);
            this.btn_AddShape.Name = "btn_AddShape";
            this.btn_AddShape.Size = new System.Drawing.Size(145, 31);
            this.btn_AddShape.TabIndex = 13;
            this.btn_AddShape.Text = "Add Shape";
            this.btn_AddShape.UseVisualStyleBackColor = true;
            this.btn_AddShape.Click += new System.EventHandler(this.btn_AddShape_Click);
            // 
            // btn_Showall
            // 
            this.btn_Showall.Location = new System.Drawing.Point(768, 330);
            this.btn_Showall.Name = "btn_Showall";
            this.btn_Showall.Size = new System.Drawing.Size(145, 31);
            this.btn_Showall.TabIndex = 14;
            this.btn_Showall.Text = "Show All Shapes";
            this.btn_Showall.UseVisualStyleBackColor = true;
            this.btn_Showall.Click += new System.EventHandler(this.btn_Showall_Click);
            // 
            // btn_Count
            // 
            this.btn_Count.Location = new System.Drawing.Point(495, 330);
            this.btn_Count.Name = "btn_Count";
            this.btn_Count.Size = new System.Drawing.Size(145, 31);
            this.btn_Count.TabIndex = 15;
            this.btn_Count.Text = "Shape Count";
            this.btn_Count.UseVisualStyleBackColor = true;
            this.btn_Count.Click += new System.EventHandler(this.btn_Count_Click);
            // 
            // Collection_of_shapes
            // 
            this.Collection_of_shapes.FormattingEnabled = true;
            this.Collection_of_shapes.Location = new System.Drawing.Point(31, 396);
            this.Collection_of_shapes.Name = "Collection_of_shapes";
            this.Collection_of_shapes.Size = new System.Drawing.Size(1044, 173);
            this.Collection_of_shapes.TabIndex = 16;
            // 
            // Shapes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 592);
            this.Controls.Add(this.Collection_of_shapes);
            this.Controls.Add(this.btn_Count);
            this.Controls.Add(this.btn_Showall);
            this.Controls.Add(this.btn_AddShape);
            this.Controls.Add(this.gr_triangle);
            this.Controls.Add(this.gr_rec);
            this.Controls.Add(this.gr_cir);
            this.Controls.Add(this.gr_squre);
            this.Controls.Add(this.cmb_typeofshapes);
            this.Controls.Add(this.labl_TypesofShape);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.lbl_shape);
            this.Name = "Shapes";
            this.Text = "Shape";
            this.gr_squre.ResumeLayout(false);
            this.gr_squre.PerformLayout();
            this.gr_cir.ResumeLayout(false);
            this.gr_cir.PerformLayout();
            this.gr_rec.ResumeLayout(false);
            this.gr_rec.PerformLayout();
            this.gr_triangle.ResumeLayout(false);
            this.gr_triangle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_shape;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label labl_TypesofShape;
        private System.Windows.Forms.ComboBox cmb_typeofshapes;
        private System.Windows.Forms.GroupBox gr_squre;
        private System.Windows.Forms.TextBox txt_side;
        private System.Windows.Forms.Label lbl_side;
        private System.Windows.Forms.GroupBox gr_cir;
        private System.Windows.Forms.TextBox txt_rad;
        private System.Windows.Forms.Label lbl_rad;
        private System.Windows.Forms.GroupBox gr_rec;
        private System.Windows.Forms.TextBox txt_bre;
        private System.Windows.Forms.Label lbl_bre;
        private System.Windows.Forms.TextBox txt_wid;
        private System.Windows.Forms.Label lbl_width;
        private System.Windows.Forms.GroupBox gr_triangle;
        private System.Windows.Forms.TextBox txt_base;
        private System.Windows.Forms.Label lbl_base;
        private System.Windows.Forms.TextBox txt_high;
        private System.Windows.Forms.Label lbl_high;
        private System.Windows.Forms.Button btn_AddShape;
        private System.Windows.Forms.Button btn_Showall;
        private System.Windows.Forms.Button btn_Count;
        private System.Windows.Forms.ListBox Collection_of_shapes;
    }
}

