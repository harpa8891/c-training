﻿using ShapeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormShapes
{
    public partial class Shapes : Form
    {
        List<Shape> shapeList;
        public Shapes()
        {
            InitializeComponent();
            DisableSetGrouBoxes();
            shapeList = new List<Shape>();
        }

        private void DisableSetGrouBoxes()
        {
            gr_cir.Enabled = false;
            gr_rec.Enabled = false;
            gr_squre.Enabled = false;
            gr_triangle.Enabled = false;
        }

        private void cmb_typeofshapes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableSetGrouBoxes();
            string name = cmb_typeofshapes.SelectedItem.ToString();
            switch (name)
            {
                case "Circle":
                    gr_cir.Enabled = true;
                    break;
                case "Rectangle":
                    gr_rec.Enabled = true;
                    break;
                case "Triangle":
                    gr_triangle.Enabled = true;
                    break;
                case "Square":
                    gr_squre.Enabled = true;
                    break;                
                default:
                    break;

            }
        }

        private void btn_AddShape_Click(object sender, EventArgs e)
        {
            string name = cmb_typeofshapes.SelectedItem.ToString();
            switch (name)
            {
                case "Circle":
                    shapeList.Add(new Circle(txt_name.Text, double.Parse(txt_rad.Text)));
                    break;
                case "Rectangle":
                    shapeList.Add(new ShapeLibrary.Rectangle(txt_name.Text, double.Parse(txt_bre.Text), double.Parse(txt_wid.Text)));
                    break;
                case "Triangle":
                    shapeList.Add(new Triangle(txt_name.Text, double.Parse(txt_high.Text), double.Parse(txt_base.Text)));
                    break;
                case "Square":
                    shapeList.Add(new Square(txt_name.Text, double.Parse(txt_side.Text)));
                    break;
                default:
                    break;

            }
            MessageBox.Show("Shape Addes into List!!!");
            txt_name.Text = string.Empty;
            txt_base.Text = string.Empty;
            txt_bre.Text = string.Empty;
            txt_high.Text = string.Empty;
            txt_rad.Text = string.Empty;
            txt_side.Text = string.Empty;
            txt_wid.Text = string.Empty;

        }

        private void btn_Showall_Click(object sender, EventArgs e)
        {
            Collection_of_shapes.Items.Clear();
            foreach(Shape shape in shapeList)
            {
                Collection_of_shapes.Items.Add(shape.GetShapeInfo() + "Total Area= " + shape.CalArea());
            }
        }

        private void btn_Count_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Shape Count: " + shapeList.Count);
        }
    }
}
