﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
    class C : IA,IB
    {
         void IA.Read()
        {
            Console.WriteLine("I1 Called");
        }        
        void IB.Read()
        {
            Console.WriteLine("I2 Called");
        }
        public void Read()
        {
            Console.WriteLine("Own function Called");
        }
    }
}
