﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            IA i1 = new C();            
            IB i2 = new C();
            C c = new C();
            c.Read();
            i1.Read();
            i2.Read();
            Console.ReadLine();
        }
    }
}
