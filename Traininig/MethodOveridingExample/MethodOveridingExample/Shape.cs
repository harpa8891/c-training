﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    abstract class Shape
    {
        public string name { get; set; }

        public Shape(string nm)
        {
            this.name = nm;
        }

        public virtual string GetShapeInfo()
        {
            return "Shape Info: " + this.name;
        }

        //public virtual double CalArea()
        //{
        //    return 0;
        //}
        public abstract double CalArea();
        
    }
}
