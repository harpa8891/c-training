﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    class Square:Shape
    {
        public double side { get; set; }
        public Square(string nm, double side):base(nm)
        {
            this.side = side;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo()+" "+this.side;
        }
        public override double CalArea()
        {
            return (this.side * this.side);
        }

    }
}
