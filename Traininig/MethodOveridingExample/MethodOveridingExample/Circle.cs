﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    class Circle:Shape
    {
        public double radius { get; set; }

        public Circle(string nm, double rad):base(nm)
        {
            this.radius = rad;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo()+" "+this.radius;
        }
        public override double CalArea()
        {
            return (3.14*this.radius*this.radius);
        }
    }
}
