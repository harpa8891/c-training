﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shape> shapes = new List<Shape>();
            shapes.Add(new Square("Square", 5));
            shapes.Add(new Triangle("Triangle", 4.5, 2.8));
            shapes.Add(new Rectangle("Rectangle", 5.8, 4.2));
            shapes.Add(new Circle("Circle", 2.8));
            shapes.Add(new Circle("Circle1", 3.8));
            shapes.Add(new Circle("Circle2", 8.8));

            foreach (Shape s in shapes)
            {
                Console.WriteLine(s.GetShapeInfo()+" Total Area: " + s.CalArea());
                
            }
        }
        static void Main1(string[] args)
        {
            
            Shape[] shapes = new Shape[4];
            shapes[0] = new Square("Square",5);
            shapes[1] = new Triangle("Triangle",4.5,2.8);
            shapes[2] = new Rectangle("Rectangle",5.8,4.2);
            shapes[3] = new Circle("Circle",2.8);

            foreach (Shape s in shapes)
            {
                Console.WriteLine(s.GetShapeInfo());
                Console.WriteLine("Total Area: "+s.CalArea());
            }
        }
    }
}
