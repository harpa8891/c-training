﻿namespace MultiThreadingWindowExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_wt = new System.Windows.Forms.Button();
            this.btn_t = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_wt
            // 
            this.btn_wt.Location = new System.Drawing.Point(38, 53);
            this.btn_wt.Name = "btn_wt";
            this.btn_wt.Size = new System.Drawing.Size(96, 23);
            this.btn_wt.TabIndex = 0;
            this.btn_wt.Text = "WithoutThread";
            this.btn_wt.UseVisualStyleBackColor = true;
            this.btn_wt.Click += new System.EventHandler(this.btn_red_Click);
            // 
            // btn_t
            // 
            this.btn_t.Location = new System.Drawing.Point(175, 53);
            this.btn_t.Name = "btn_t";
            this.btn_t.Size = new System.Drawing.Size(75, 23);
            this.btn_t.TabIndex = 1;
            this.btn_t.Text = "WithThread";
            this.btn_t.UseVisualStyleBackColor = true;
            this.btn_t.Click += new System.EventHandler(this.btn_t_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 218);
            this.Controls.Add(this.btn_t);
            this.Controls.Add(this.btn_wt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_wt;
        private System.Windows.Forms.Button btn_t;
    }
}

