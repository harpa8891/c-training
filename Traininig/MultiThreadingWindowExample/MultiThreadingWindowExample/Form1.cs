﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace MultiThreadingWindowExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Thread th;
        Thread th1;
        
        Random rdm;

        private void btn_red_Click(object sender, EventArgs e)
        {
            ThreadBlue();
            ThreadRed();
            
        }
        public void ThreadRed()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Orange, 4), new Rectangle(rdm.Next(0, this.Width), rdm.Next(0, this.Height),20,20));
                Thread.Sleep(10);
            }
            MessageBox.Show("Completed red");
        }
        public void ThreadBlue()
        {
            for (int i = 0; i < 100; i++)
            {
                this.CreateGraphics().DrawRectangle(new Pen(Brushes.Blue, 4), new Rectangle(rdm.Next(0, this.Width), rdm.Next(0, this.Height), 20, 20));
                Thread.Sleep(10);
            }
            MessageBox.Show("Completed Blue");
        }        

        private void Form1_Load(object sender, EventArgs e)
        {
            rdm = new Random();
        }        
        private void btn_t_Click(object sender, EventArgs e)
        {
            this.CreateGraphics().Clear(Color.White);
            Thread.Sleep(100);        
            th = new Thread(ThreadRed);
            th.Start();
            th1 = new Thread(ThreadBlue);
            th1.Start();
        }
    }
}
