﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingExample
{
    class Class1
    {
        public static void Fun()
        {
            Console.WriteLine("in static function");
        }
        public  void MyFun()
        {
            Console.WriteLine("in Non-static function");
        }
    }
}
