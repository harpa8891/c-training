﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingExample
{
    class Program
    {
        static void Main1(string[] args)
        {
            Console.WriteLine("App Starts...");
            int x = 1, y = 1;
            string val = "123";
            Class1 cl = null;
            try
            {
                Class1.Fun();
                Console.WriteLine("enter name");
                string name = Console.ReadLine();
                if (name.Equals("a")) ;
                {
                    throw new Exception("not valid");
                }
                x = x / y;
                Console.WriteLine("Value of x: " + x);
                y = int.Parse(val);
                Console.WriteLine("Value of y: " + y);
            }
            catch (DivideByZeroException divEx)
            {
                Console.WriteLine("Error: " + divEx.Message);
            }
            catch (FormatException f)
            {
                Console.WriteLine("Error: " + f.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                Console.WriteLine("enter in finally");
            }
            Console.WriteLine("App Ends....");

        }
        static void Main(string[] args)
        {

            Console.Write("Enter Number 1: ");
            int n1 = int.Parse(Console.ReadLine());
            Console.Write("Enter Number 2: ");
            int n2 = int.Parse(Console.ReadLine());
            try
            {
                int res = n1 / n2;

            }
            catch(FormatException exp)
            {
                Console.WriteLine(exp.Message);
            }
            catch(NullReferenceException nexp)
            {
                Console.WriteLine(nexp.Message);
            }
           
            finally
            {
                Console.WriteLine("5");
            }
            Console.WriteLine("6");
        }
    }
}
