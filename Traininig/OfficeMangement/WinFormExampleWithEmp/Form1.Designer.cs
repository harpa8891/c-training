﻿namespace WinFormExampleWithEmp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_id = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.lbl_Sal = new System.Windows.Forms.Label();
            this.txt_Id = new System.Windows.Forms.TextBox();
            this.txt_sal = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.labl_Typesofemp = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.gr_eng = new System.Windows.Forms.GroupBox();
            this.txt_incentive = new System.Windows.Forms.TextBox();
            this.txt_extrahrs = new System.Windows.Forms.TextBox();
            this.lbl_incentive = new System.Windows.Forms.Label();
            this.lbl_eng = new System.Windows.Forms.Label();
            this.gr_man = new System.Windows.Forms.GroupBox();
            this.txt_Bonus = new System.Windows.Forms.TextBox();
            this.txt_Dept = new System.Windows.Forms.TextBox();
            this.lbl_bonus = new System.Windows.Forms.Label();
            this.lbl_dept = new System.Windows.Forms.Label();
            this.gr_sales = new System.Windows.Forms.GroupBox();
            this.txt_comm = new System.Windows.Forms.TextBox();
            this.txt_sales = new System.Windows.Forms.TextBox();
            this.lbl_comm = new System.Windows.Forms.Label();
            this.lbl_sales = new System.Windows.Forms.Label();
            this.btn_AddEmp = new System.Windows.Forms.Button();
            this.btn_Showall = new System.Windows.Forms.Button();
            this.btn_Count = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_showempfromfile = new System.Windows.Forms.Button();
            this.btn_addempfile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.gr_eng.SuspendLayout();
            this.gr_man.SuspendLayout();
            this.gr_sales.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.Location = new System.Drawing.Point(85, 23);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(21, 16);
            this.lbl_id.TabIndex = 0;
            this.lbl_id.Text = "ID";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Name.Location = new System.Drawing.Point(85, 61);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(45, 16);
            this.lbl_Name.TabIndex = 1;
            this.lbl_Name.Text = "Name";
            // 
            // lbl_Sal
            // 
            this.lbl_Sal.AutoSize = true;
            this.lbl_Sal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Sal.Location = new System.Drawing.Point(85, 106);
            this.lbl_Sal.Name = "lbl_Sal";
            this.lbl_Sal.Size = new System.Drawing.Size(47, 16);
            this.lbl_Sal.TabIndex = 2;
            this.lbl_Sal.Text = "Salary";
            // 
            // txt_Id
            // 
            this.txt_Id.Location = new System.Drawing.Point(172, 23);
            this.txt_Id.Name = "txt_Id";
            this.txt_Id.Size = new System.Drawing.Size(100, 20);
            this.txt_Id.TabIndex = 3;
            // 
            // txt_sal
            // 
            this.txt_sal.Location = new System.Drawing.Point(172, 106);
            this.txt_sal.Name = "txt_sal";
            this.txt_sal.Size = new System.Drawing.Size(100, 20);
            this.txt_sal.TabIndex = 4;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(172, 61);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 20);
            this.txt_name.TabIndex = 5;
            // 
            // labl_Typesofemp
            // 
            this.labl_Typesofemp.AutoSize = true;
            this.labl_Typesofemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labl_Typesofemp.Location = new System.Drawing.Point(28, 149);
            this.labl_Typesofemp.Name = "labl_Typesofemp";
            this.labl_Typesofemp.Size = new System.Drawing.Size(133, 16);
            this.labl_Typesofemp.TabIndex = 6;
            this.labl_Typesofemp.Text = "Types of Employees";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Sales",
            "Manager",
            "Engineer"});
            this.comboBox1.Location = new System.Drawing.Point(172, 144);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.Text = "Select One";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // gr_eng
            // 
            this.gr_eng.Controls.Add(this.txt_incentive);
            this.gr_eng.Controls.Add(this.txt_extrahrs);
            this.gr_eng.Controls.Add(this.lbl_incentive);
            this.gr_eng.Controls.Add(this.lbl_eng);
            this.gr_eng.Location = new System.Drawing.Point(31, 178);
            this.gr_eng.Name = "gr_eng";
            this.gr_eng.Size = new System.Drawing.Size(219, 111);
            this.gr_eng.TabIndex = 8;
            this.gr_eng.TabStop = false;
            this.gr_eng.Text = "Engineer";
            // 
            // txt_incentive
            // 
            this.txt_incentive.Location = new System.Drawing.Point(90, 74);
            this.txt_incentive.Name = "txt_incentive";
            this.txt_incentive.Size = new System.Drawing.Size(100, 20);
            this.txt_incentive.TabIndex = 10;
            // 
            // txt_extrahrs
            // 
            this.txt_extrahrs.Location = new System.Drawing.Point(90, 32);
            this.txt_extrahrs.Name = "txt_extrahrs";
            this.txt_extrahrs.Size = new System.Drawing.Size(100, 20);
            this.txt_extrahrs.TabIndex = 9;
            // 
            // lbl_incentive
            // 
            this.lbl_incentive.AutoSize = true;
            this.lbl_incentive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_incentive.Location = new System.Drawing.Point(17, 74);
            this.lbl_incentive.Name = "lbl_incentive";
            this.lbl_incentive.Size = new System.Drawing.Size(61, 16);
            this.lbl_incentive.TabIndex = 9;
            this.lbl_incentive.Text = "Incentive";
            // 
            // lbl_eng
            // 
            this.lbl_eng.AutoSize = true;
            this.lbl_eng.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_eng.Location = new System.Drawing.Point(17, 32);
            this.lbl_eng.Name = "lbl_eng";
            this.lbl_eng.Size = new System.Drawing.Size(62, 16);
            this.lbl_eng.TabIndex = 3;
            this.lbl_eng.Text = "Extra Hrs";
            // 
            // gr_man
            // 
            this.gr_man.Controls.Add(this.txt_Bonus);
            this.gr_man.Controls.Add(this.txt_Dept);
            this.gr_man.Controls.Add(this.lbl_bonus);
            this.gr_man.Controls.Add(this.lbl_dept);
            this.gr_man.Location = new System.Drawing.Point(332, 178);
            this.gr_man.Name = "gr_man";
            this.gr_man.Size = new System.Drawing.Size(219, 111);
            this.gr_man.TabIndex = 9;
            this.gr_man.TabStop = false;
            this.gr_man.Text = "Manager";
            // 
            // txt_Bonus
            // 
            this.txt_Bonus.Location = new System.Drawing.Point(90, 74);
            this.txt_Bonus.Name = "txt_Bonus";
            this.txt_Bonus.Size = new System.Drawing.Size(100, 20);
            this.txt_Bonus.TabIndex = 10;
            // 
            // txt_Dept
            // 
            this.txt_Dept.Location = new System.Drawing.Point(90, 32);
            this.txt_Dept.Name = "txt_Dept";
            this.txt_Dept.Size = new System.Drawing.Size(100, 20);
            this.txt_Dept.TabIndex = 9;
            // 
            // lbl_bonus
            // 
            this.lbl_bonus.AutoSize = true;
            this.lbl_bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bonus.Location = new System.Drawing.Point(17, 74);
            this.lbl_bonus.Name = "lbl_bonus";
            this.lbl_bonus.Size = new System.Drawing.Size(46, 16);
            this.lbl_bonus.TabIndex = 9;
            this.lbl_bonus.Text = "Bonus";
            // 
            // lbl_dept
            // 
            this.lbl_dept.AutoSize = true;
            this.lbl_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dept.Location = new System.Drawing.Point(17, 32);
            this.lbl_dept.Name = "lbl_dept";
            this.lbl_dept.Size = new System.Drawing.Size(37, 16);
            this.lbl_dept.TabIndex = 3;
            this.lbl_dept.Text = "Dept";
            // 
            // gr_sales
            // 
            this.gr_sales.Controls.Add(this.txt_comm);
            this.gr_sales.Controls.Add(this.txt_sales);
            this.gr_sales.Controls.Add(this.lbl_comm);
            this.gr_sales.Controls.Add(this.lbl_sales);
            this.gr_sales.Location = new System.Drawing.Point(624, 178);
            this.gr_sales.Name = "gr_sales";
            this.gr_sales.Size = new System.Drawing.Size(219, 111);
            this.gr_sales.TabIndex = 10;
            this.gr_sales.TabStop = false;
            this.gr_sales.Text = "Sales Person";
            // 
            // txt_comm
            // 
            this.txt_comm.Location = new System.Drawing.Point(90, 74);
            this.txt_comm.Name = "txt_comm";
            this.txt_comm.Size = new System.Drawing.Size(100, 20);
            this.txt_comm.TabIndex = 10;
            // 
            // txt_sales
            // 
            this.txt_sales.Location = new System.Drawing.Point(90, 32);
            this.txt_sales.Name = "txt_sales";
            this.txt_sales.Size = new System.Drawing.Size(100, 20);
            this.txt_sales.TabIndex = 9;
            // 
            // lbl_comm
            // 
            this.lbl_comm.AutoSize = true;
            this.lbl_comm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_comm.Location = new System.Drawing.Point(17, 74);
            this.lbl_comm.Name = "lbl_comm";
            this.lbl_comm.Size = new System.Drawing.Size(47, 16);
            this.lbl_comm.TabIndex = 9;
            this.lbl_comm.Text = "Comm";
            // 
            // lbl_sales
            // 
            this.lbl_sales.AutoSize = true;
            this.lbl_sales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sales.Location = new System.Drawing.Point(17, 32);
            this.lbl_sales.Name = "lbl_sales";
            this.lbl_sales.Size = new System.Drawing.Size(43, 16);
            this.lbl_sales.TabIndex = 3;
            this.lbl_sales.Text = "Sales";
            // 
            // btn_AddEmp
            // 
            this.btn_AddEmp.Location = new System.Drawing.Point(16, 349);
            this.btn_AddEmp.Name = "btn_AddEmp";
            this.btn_AddEmp.Size = new System.Drawing.Size(145, 31);
            this.btn_AddEmp.TabIndex = 11;
            this.btn_AddEmp.Text = "Add Employee";
            this.btn_AddEmp.UseVisualStyleBackColor = true;
            this.btn_AddEmp.Click += new System.EventHandler(this.btn_AddEmp_Click);
            // 
            // btn_Showall
            // 
            this.btn_Showall.Location = new System.Drawing.Point(199, 349);
            this.btn_Showall.Name = "btn_Showall";
            this.btn_Showall.Size = new System.Drawing.Size(145, 31);
            this.btn_Showall.TabIndex = 12;
            this.btn_Showall.Text = "Show All Employees";
            this.btn_Showall.UseVisualStyleBackColor = true;
            this.btn_Showall.Click += new System.EventHandler(this.btn_Showall_Click);
            // 
            // btn_Count
            // 
            this.btn_Count.Location = new System.Drawing.Point(377, 349);
            this.btn_Count.Name = "btn_Count";
            this.btn_Count.Size = new System.Drawing.Size(145, 31);
            this.btn_Count.TabIndex = 13;
            this.btn_Count.Text = "Employee Count";
            this.btn_Count.UseVisualStyleBackColor = true;
            this.btn_Count.Click += new System.EventHandler(this.btn_Count_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(31, 409);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(812, 173);
            this.listBox1.TabIndex = 14;
            // 
            // btn_showempfromfile
            // 
            this.btn_showempfromfile.Location = new System.Drawing.Point(737, 349);
            this.btn_showempfromfile.Name = "btn_showempfromfile";
            this.btn_showempfromfile.Size = new System.Drawing.Size(145, 31);
            this.btn_showempfromfile.TabIndex = 15;
            this.btn_showempfromfile.Text = "show emps from file";
            this.btn_showempfromfile.UseVisualStyleBackColor = true;
            this.btn_showempfromfile.Click += new System.EventHandler(this.btn_showempfromfile_Click);
            // 
            // btn_addempfile
            // 
            this.btn_addempfile.Location = new System.Drawing.Point(558, 349);
            this.btn_addempfile.Name = "btn_addempfile";
            this.btn_addempfile.Size = new System.Drawing.Size(145, 31);
            this.btn_addempfile.TabIndex = 16;
            this.btn_addempfile.Text = "Add emps in file";
            this.btn_addempfile.UseVisualStyleBackColor = true;
            this.btn_addempfile.Click += new System.EventHandler(this.btn_addempfile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(377, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 20);
            this.button1.TabIndex = 17;
            this.button1.Text = "Browse File Path";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(528, 301);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(315, 20);
            this.textBox1.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 595);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_addempfile);
            this.Controls.Add(this.btn_showempfromfile);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btn_Count);
            this.Controls.Add(this.btn_Showall);
            this.Controls.Add(this.btn_AddEmp);
            this.Controls.Add(this.gr_sales);
            this.Controls.Add(this.gr_man);
            this.Controls.Add(this.gr_eng);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labl_Typesofemp);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.txt_sal);
            this.Controls.Add(this.txt_Id);
            this.Controls.Add(this.lbl_Sal);
            this.Controls.Add(this.lbl_Name);
            this.Controls.Add(this.lbl_id);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gr_eng.ResumeLayout(false);
            this.gr_eng.PerformLayout();
            this.gr_man.ResumeLayout(false);
            this.gr_man.PerformLayout();
            this.gr_sales.ResumeLayout(false);
            this.gr_sales.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label lbl_Sal;
        private System.Windows.Forms.TextBox txt_Id;
        private System.Windows.Forms.TextBox txt_sal;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label labl_Typesofemp;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox gr_eng;
        private System.Windows.Forms.TextBox txt_incentive;
        private System.Windows.Forms.TextBox txt_extrahrs;
        private System.Windows.Forms.Label lbl_incentive;
        private System.Windows.Forms.Label lbl_eng;
        private System.Windows.Forms.GroupBox gr_man;
        private System.Windows.Forms.TextBox txt_Bonus;
        private System.Windows.Forms.TextBox txt_Dept;
        private System.Windows.Forms.Label lbl_bonus;
        private System.Windows.Forms.Label lbl_dept;
        private System.Windows.Forms.GroupBox gr_sales;
        private System.Windows.Forms.TextBox txt_comm;
        private System.Windows.Forms.TextBox txt_sales;
        private System.Windows.Forms.Label lbl_comm;
        private System.Windows.Forms.Label lbl_sales;
        private System.Windows.Forms.Button btn_AddEmp;
        private System.Windows.Forms.Button btn_Showall;
        private System.Windows.Forms.Button btn_Count;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_showempfromfile;
        private System.Windows.Forms.Button btn_addempfile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

