﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class Engg:Employee
    {
        double extraHrs;
        double incen;
        public Engg(int id,string nm,double sal,double exHr,double inc) : base(id,nm,sal)
        {
            this.extraHrs = exHr;
            this.incen = inc;
        }
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.extraHrs + " " + this.incen+" ---eng";
        }
        public override double CalSal()
        {
            return this.salary + (this.incen*this.extraHrs);
        }
    }
}
