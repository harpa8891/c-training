﻿using OfficeMangement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormExampleWithEmp
{
    public partial class Form1 : Form
    {
        List<Employee> empList;
        public Form1()
        {
            InitializeComponent();
            DisableSetGrouBoxes();
            empList = new List<Employee>();
        }
        public void DisableSetGrouBoxes()
        {
            gr_eng.Enabled = false;
            gr_man.Enabled = false;
            gr_sales.Enabled = false;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableSetGrouBoxes();
            string name = comboBox1.SelectedItem.ToString();
            switch(name)
            {
                case "Manager":
                    gr_man.Enabled = true;
                    break;
                case "Engineer":
                    gr_eng.Enabled = true;                    
                    break;
                case "Sales":
                    gr_sales.Enabled = true;                   
                    break;
                default:
                    break;

            }            
        }

        private void btn_AddEmp_Click(object sender, EventArgs e)
        {
            string name = comboBox1.SelectedItem.ToString();
            switch (name)
            {
                case "Manager":
                    empList.Add(new Manager(int.Parse(txt_Id.Text), txt_name.Text, double.Parse(txt_sal.Text), double.Parse(txt_Bonus.Text), txt_Dept.Text));
                    break;
                case "Engineer":
                    empList.Add(new Engg(int.Parse(txt_Id.Text), txt_name.Text,double.Parse(txt_sal.Text), double.Parse(txt_extrahrs.Text), double.Parse(txt_incentive.Text)));
                    break;
                case "Sales":
                    empList.Add(new Sales(int.Parse(txt_Id.Text), txt_name.Text, double.Parse(txt_sal.Text),int.Parse(txt_sales.Text), double.Parse(txt_comm.Text)));
                    break;
                default:
                    break;

            }
            MessageBox.Show("Emp Addes into List!!!");
            ResetAllTextBox();
            
        }
        private void ResetAllTextBox()
        {
            txt_Id.Text = string.Empty;
            txt_Bonus.Text = string.Empty;
            txt_comm.Text = string.Empty;
            txt_Dept.Text = string.Empty;
            txt_extrahrs.Text = string.Empty;
            txt_incentive.Text = string.Empty;
            txt_name.Text = string.Empty;
            txt_sal.Text = string.Empty;
            txt_sales.Text = string.Empty;
        }
        private void btn_Showall_Click(object sender, EventArgs e)
        {

            listBox1.Items.Clear();
            foreach(Employee emp in empList)
            {
                listBox1.Items.Add(emp.GetEmpInfo()+"Total Sal= "+emp.CalSal());
            }
        }

        private void btn_Count_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Emp Count: " + empList.Count);
        }
      
        private void btn_addempfile_Click(object sender, EventArgs e)
        {
            if(textBox1.Text.Length<3)
            {
                MessageBox.Show("No File Selected yet !");
                return;
            }
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                string fname = textBox1.Text;
                if(!File.Exists(fname))
                {
                    MessageBox.Show("File doesn't exist !");
                    return;
                }
                fs = new FileStream(textBox1.Text, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
               // fs = new FileStream(@"C:\harshad\temp\emp.txt", FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                sw = new StreamWriter(fs);

                foreach (Employee emp in empList)
                {
                    sw.WriteLine(emp.GetEmpInfo() + " Total Sal= " + emp.CalSal());
                }

                MessageBox.Show("Emps saved into File!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();
            }            
        }

        private void btn_showempfromfile_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 3)
            {
                MessageBox.Show("No File Selected yet !");
                return;
            }
            listBox1.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                fs = new FileStream(@"C:\harshad\temp\emp.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while((line=sr.ReadLine())!=null)
                {
                    listBox1.Items.Add(line);
                }

                MessageBox.Show("Emps saved into File!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
            }
            ResetAllTextBox();

        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }
    }
}
