﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class Sales:Employee
    {
        int sales;
        double commition;
        public Sales(int id,string name,double salary,int sales,double commition) :base(id,name,salary)
        {
            this.sales = sales;
            this.commition = commition;
        }
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.sales + " " + this.commition + " ---sp";
        }
        public override double CalSal()
        {
            return this.salary + (this.sales * this.commition);
        }
    }
}
