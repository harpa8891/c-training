﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class Manager:Employee
    {
        string dept;
        double bonus;
        public Manager(int id,string nm,double sal,double bonus,string dept) : base(id,nm, sal)
        {
            this.dept = dept;
            this.bonus = bonus;
        }
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + " " + this.dept + " " + this.bonus + " ---Mgr";
        }
        public override double CalSal()
        {
            return this.salary + (this.bonus);
        }
    }
}
