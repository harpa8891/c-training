﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class Program
    {
        static void Main1(string[] args)
        {
            
            Console.WriteLine("Emp count {0}",Employee.GetCount());
            Employee.SetCompanyName("Tricentis India");
            Employee emp = new Employee();
            //emp.SetEmpInfo(101, "Harshad", 12457);

            Employee emp1 = new Employee(103, "Pranita", 12348);

            Employee emp2 = new Employee(emp1);

            Employee emp3 = new Employee("Chetan",104, 12348);

            Console.WriteLine(emp.GetEmpInfo());
            Console.WriteLine(emp1.GetEmpInfo());
            Console.WriteLine(emp3.GetEmpInfo());
            Console.WriteLine("Emp count {0}", Employee.GetCount());
            Console.ReadKey();
        }
        static void Main2(string[] args)
        {
            MyUtility m = new MyUtility();
            int[] input = { 1, 2, 3 };
            Console.WriteLine("Addition of Two Number: " + m.Add(5, 7));
            Console.WriteLine("Addition of Three Number: " + m.Add(5, 7, 8));
            Console.WriteLine("Concat of Two String: " + m.Add("Harshad", "Patel"));
            Console.WriteLine("Addition of Array Element: "+m.AddArray(input));

        }
        static void Main3(string[] args)
        {
            EmployeeList.AddEmployees();
            Employee[] myEMp = EmployeeList.GetEmp();
            for (int i = 0; i < myEMp.Length; i++)
            {
                Console.WriteLine(myEMp[i].GetEmpInfo());
            }
            foreach(Employee e in myEMp)
            {
                Console.WriteLine(e.GetEmpInfo());
            }

        }
        static void Main4(string[] args)
        {
            //method hiding 
            Sales s1 = new Sales(101, "Harshad", 12345, 50,100);
            Engg e1 = new Engg(102, "Nilesh", 12341, 12, 125);
            Manager m1 = new Manager(103, "Pranita", 12365, 52, "eng");

            Console.WriteLine(s1.GetEmpInfo());
            Console.WriteLine("Total sal= "+s1.CalSal());
            Console.WriteLine(e1.GetEmpInfo());
            Console.WriteLine("Total sal= " + e1.CalSal());
            Console.WriteLine(m1.GetEmpInfo());
            Console.WriteLine("Total sal= " + m1.CalSal());


        }
        static void Main5(string[] args)
        {
            //inheritance
            Employee e1 = new Sales(101, "Harshad", 12345, 50, 100);
            
            Employee e2 = new Engg(102, "Nilesh", 12341, 12, 125);
           
            Employee e3 = new Manager(103, "Pranita", 12365, 52, "eng");

            Console.WriteLine(e1.GetEmpInfo());
            Console.WriteLine("Total sal= " + e1.CalSal());
            Console.WriteLine(e2.GetEmpInfo());
            Console.WriteLine("Total sal= " + e2.CalSal());
            Console.WriteLine(e3.GetEmpInfo());
            Console.WriteLine("Total sal= " + e3.CalSal());


        }
        static void Main(string[] args)
        {
            
            //Polimorphism
            Employee[] err = new Employee[3];
            err[0]= new Sales(101, "Harshad", 12345, 50, 100);
            err[1]= new Engg(102, "Nilesh", 12341, 12, 125);
            err[2]= new Manager(103, "Pranita", 12365, 52, "eng");
            
            foreach(Employee emp in err)
            {
                Console.WriteLine(emp.GetEmpInfo());
                Console.WriteLine("Total sal= " + emp.CalSal());
            }                        
        }
        //Pen p1 = new Pen();
        //Console.WriteLine(p1.GetInfo());         
        //p1.SetInfo("Green", "Reynolds", false, 0.7);            
        //Pen p2 = new Pen("Red", "Parker", true, 0.5,100);
        //Console.WriteLine(p2.GetInfo());
        //Pen p3 = new Pen(p1);
        //Console.WriteLine(p3.GetInfo());
        //Console.WriteLine("\nAfter Setting Info"+p1.GetInfo());
        //Console.WriteLine("\nPen Price is: "+p2.GetPrice());
        //p1.GetDiscount(25);
        //p1.GetPrice();

        //Console.WriteLine("\nFinal Price of Pen is {0}", p1.price);
    }
}
