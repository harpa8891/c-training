﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class MyUtility
    {
        public int Add(int n1, int n2)
        {
            return n1 + n2;
        }

        public int Add(int n1, int n2,int n3)
        {
            return n1 + n2+n3;
        }

        public string Add(string s1, string s2)
        {
            return s1 + s2;
        }

        public int AddArray(int[]input)
        {
            return input.Sum();
        }

    }
}
