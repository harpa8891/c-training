﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeMangement
{
    class EmployeeList
    {
        static Employee[] empList;
         static EmployeeList()
        {
            empList = new Employee[3];
        }
        public static void AddEmployees()
        {
            empList[0] = new Employee(105, "aaa", 12345);
            empList[1] = new Employee(106, "bbb", 15423);
            empList[2] = new Employee(107, "ccc", 13245);
        }
        public static Employee[] GetEmp()
        {
            return empList;
        }
    }
}
