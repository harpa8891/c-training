﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training
{
    class Program
    {
        static void Main(string[] args)
        {
            //string s = "hi";
            //s[1] = 'a';   ///Compilation Error.

            //string s = "abc";
            //int i = 0;
            //int.TryParse(s,out i); //Exception Handel inside
            //Console.Write(i);
            string date = "25/12/2017";
            DateTime dt1;
            DateTime.TryParse(date, out dt1);

            string dateFormatOdataService = dt1.ToString("MM/dd/yyyy");

            
            string ans= Convert.ToDateTime(date, CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");


        }
    }
}
