﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADO_Net_Example
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SqlConnection con = null;


        private void btn_dbData_Click(object sender, EventArgs e)
        {
            
            try
            {
                con = new SqlConnection(@"Data Source=TRI-NB0639\SQLEXPRESS;Initial Catalog=Training;Persist Security Info=True;User ID=hspatel;Password=qazwsx123");
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * From Employee;", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    MessageBox.Show("Employee Info= " + sdr[0] + " " + sdr["Name"] + " " + sdr.GetDecimal(2) + " " + sdr.GetString(3));
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                con = new SqlConnection(@"Data Source=TRI-NB0639\SQLEXPRESS;Initial Catalog=Training;Persist Security Info=True;User ID=hspatel;Password=qazwsx123");
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * From Employee;", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sdr);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                con = new SqlConnection(@"Data Source=TRI-NB0639\SQLEXPRESS;Initial Catalog=Training;Persist Security Info=True;User ID=hspatel;Password=qazwsx123");
                con.Open();
                SqlCommand cmd = new SqlCommand("Select count(*) From Employee;", con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                MessageBox.Show("count "+count);                             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
