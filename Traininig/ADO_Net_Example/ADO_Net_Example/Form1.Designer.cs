﻿namespace ADO_Net_Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_dbData = new System.Windows.Forms.Button();
            this.btn_gridview = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.trainingDataSet = new ADO_Net_Example.TrainingDataSet();
            this.trainingDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btn_count = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainingDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainingDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_dbData
            // 
            this.btn_dbData.Location = new System.Drawing.Point(108, 107);
            this.btn_dbData.Name = "btn_dbData";
            this.btn_dbData.Size = new System.Drawing.Size(133, 23);
            this.btn_dbData.TabIndex = 0;
            this.btn_dbData.Text = "Get Data From DB";
            this.btn_dbData.UseVisualStyleBackColor = true;
            this.btn_dbData.Click += new System.EventHandler(this.btn_dbData_Click);
            // 
            // btn_gridview
            // 
            this.btn_gridview.Location = new System.Drawing.Point(544, 107);
            this.btn_gridview.Name = "btn_gridview";
            this.btn_gridview.Size = new System.Drawing.Size(197, 23);
            this.btn_gridview.TabIndex = 1;
            this.btn_gridview.Text = "Get Data  From DB show in gridview";
            this.btn_gridview.UseVisualStyleBackColor = true;
            this.btn_gridview.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(108, 213);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(633, 272);
            this.dataGridView1.TabIndex = 2;
            // 
            // trainingDataSet
            // 
            this.trainingDataSet.DataSetName = "TrainingDataSet";
            this.trainingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // trainingDataSetBindingSource
            // 
            this.trainingDataSetBindingSource.DataSource = this.trainingDataSet;
            this.trainingDataSetBindingSource.Position = 0;
            // 
            // btn_count
            // 
            this.btn_count.Location = new System.Drawing.Point(333, 107);
            this.btn_count.Name = "btn_count";
            this.btn_count.Size = new System.Drawing.Size(132, 23);
            this.btn_count.TabIndex = 3;
            this.btn_count.Text = "Show Employee Count";
            this.btn_count.UseVisualStyleBackColor = true;
            this.btn_count.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 595);
            this.Controls.Add(this.btn_count);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_gridview);
            this.Controls.Add(this.btn_dbData);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainingDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trainingDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_dbData;
        private System.Windows.Forms.Button btn_gridview;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource trainingDataSetBindingSource;
        private TrainingDataSet trainingDataSet;
        private System.Windows.Forms.Button btn_count;
    }
}

