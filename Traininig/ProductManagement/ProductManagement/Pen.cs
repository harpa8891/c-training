﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement
{
    public class Pen
    {
        public string colour { get; set; }

        public string brandName { get; set; }

        public bool isFountain { get; set; }

        public double price { get; set; }

        public double width { get; set; }

        public static int count { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Pen()
        {
            
            this.colour = "Blue";
            this.brandName = "Cello";
            this.isFountain = true;
            this.width = 0.6;
            this.price = 100;

        }
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="col">Pen Colour</param>
        /// <param name="brand">Pen Comapny Name</param>
        /// <param name="fountain">Is Fountain Or Not</param>
        /// <param name="wid">Pen width</param>
        public Pen(string col, string brand,bool fountain,double wid,double price)
        {
            
            this.colour = col;
            this.brandName = brand;
            this.isFountain = fountain;
            this.width = wid;
            this.price = price;
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p">Object Of Pen</param>
        public Pen(Pen p)
        {
            
            this.colour = p.colour;
            this.brandName = p.brandName;
            this.isFountain = p.isFountain;
            this.width = p.width;
            this.price = p.price;
        }

        /// <summary>
        /// Set Information About Pen
        /// </summary>
        /// <param name="col">Pen Colour</param>
        /// <param name="brand">Pen Comapny Name</param>
        /// <param name="fountain">Is Fountain Or Not</param>
        /// <param name="wid">Pen width</param>
        public void SetInfo(string col, string brand, bool fountain, double wid)
        {
            this.colour = col;
            this.brandName = brand;
            this.isFountain = fountain;
            this.width = wid;
        }
        /// <summary>
        /// Getting information of Pen
        /// </summary>
        /// <returns></returns>
        public string GetInfo()
        {
            string res = isFountain ? "Yes" : "No";
            return "\nPen Details: \nPen Colour is " + colour + "\nPen Brand is " + brandName + "\nPen Width is " + width + "\nPen Is Fountain? " + res+"\nPen Price is "+price;
        }
        /// <summary>
        /// Get Price Of Pen
        /// </summary>
        /// <returns>Price Of Pen</returns>
        public double GetPrice()
        {
            return price;
        }
        /// <summary>
        /// Get Discount
        /// </summary>
        /// <param name="percentage">Percentage of Discount</param>
        public void GetDiscount(double percentage)
        {
            this.price = price - (price * percentage / 100);
            
        }
        public static int GetCount()
        {
            return count;
        }
    }
}
