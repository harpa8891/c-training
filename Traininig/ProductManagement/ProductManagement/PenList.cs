﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement
{
    public class PenList
    {
        static Pen[] penList;
        static PenList()
        {
            penList = new Pen[3];
        }
        public static void AddPens()
        {
            penList[0] = new Pen("Blue", "Cello", true, 0.6, 100);
            penList[1] = new Pen("Red", "Cello", false, 0.8, 150);
            penList[2] = new Pen("Green", "Cello", true, 0.7, 180);
        }
        public static Pen[] GetPen()
        {
            return penList;
        }
    }
}
