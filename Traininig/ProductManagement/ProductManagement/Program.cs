﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Emp count {0}",Pen.GetCount()); 
            PenList.AddPens();
            Pen[] Pens = PenList.GetPen();
            
            
            foreach (Pen e in Pens)
            {
                Console.WriteLine(e.GetInfo());
            }

            Console.WriteLine("After Discount");
            foreach (Pen e in Pens)
            {
                e.GetDiscount(20);
                Console.WriteLine(e.GetInfo());
            }
            Console.ReadLine();


        }
    }
}
