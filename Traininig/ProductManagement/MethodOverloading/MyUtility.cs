﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverloading
{
    class MyUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        public void Add(int n1,int n2)
        {

        }

        //Concept Of Method Overloading
        /// <summary>
        /// Calculate Salary
        /// </summary>
        /// <param name="basic">basic sal</param>
        /// <param name="hra">HRA</param>
        /// <param name="pf">PF</param>
        /// <returns>salary</returns>
        public double CalculateSalary(int basic,int hra,int pf)
        {
            return ((basic + hra) - ((12 * basic) / 100));
        }
        /// <summary>
        /// Calculate Salary
        /// </summary>
        /// <param name="basic">basic sal</param>
        /// <param name="hra">HRA</param>
        /// <param name="pf">PF</param>
        /// <param name="medical">Medical Allowance</param>
        /// <returns>salary</returns>
        public double CalculateSalary(int basic, int hra, int pf,int medical)
        {
            return ((basic + hra+medical) - ((12 * basic) / 100));
        }
        /// <summary>
        /// Calculate Salary
        /// </summary>
        /// <param name="basic">basic sal</param>
        /// <param name="hra">HRA</param>
        /// <param name="pf">PF</param>
        /// <param name="medical">Mediacal Allowance</param>
        /// <param name="pt">Professional Text</param>
        /// <returns></returns>
        public double CalculateSalary(int basic, int hra, int pf, int medical,int pt)
        {
            return ((basic + hra + medical) - (((12 * basic)+pt) / 100));
        }
    }
}
