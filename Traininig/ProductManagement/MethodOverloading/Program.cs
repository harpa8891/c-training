﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverloading
{
    class Program
    {
        static void Main(string[] args)
        {
            MyUtility mu = new MyUtility();
            
            Console.WriteLine("Net Slaray of Employee: "+mu.CalculateSalary(33000, 1500, 2500));
            Console.WriteLine("Net Slaray of Employee: " + mu.CalculateSalary(33000, 1500, 2500,1800));
            Console.WriteLine("Net Slaray of Employee: " + mu.CalculateSalary(33000, 1500, 2500, 1800,200));
        }
    }
}
