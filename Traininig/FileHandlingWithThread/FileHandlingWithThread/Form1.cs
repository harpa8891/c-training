﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileHandlingWithThread
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Thread th;
        Thread th1;
        //Thread th2;
        Thread th3;
        Thread th4;

        private void btn_browse_read_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txt_browse_read.Text = openFileDialog1.FileName;
            }
        }

        private void ReadData()
        {
            if (txt_browse_read.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !!!!");
                return;
            }
            listBox_read.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                fs = new FileStream(txt_browse_read.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listBox_read.Items.Add(line);
                    
                    //Thread.Sleep(3);
                    //th2=new Thread(()=>WriteIntoListandFile(line));
                    //th2.Start();
                    //Thread.Sleep(3);
                }

                MessageBox.Show("data read from File!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
            }
        }



        private void WriteData()
        {
            if (txt_browse_write.Text.Length < 3)
            {
                MessageBox.Show("No File Selected yet !");
                return;
            }
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                string fname = txt_browse_write.Text;
                if (!File.Exists(fname))
                {
                    fname = fname + ".txt";
                    File.Create(fname).Dispose();
                    using (TextWriter tw = new StreamWriter(fname))
                    {
                        tw.Close();
                    }

                }
                fs = new FileStream(fname, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);

                sw = new StreamWriter(fs);

                for (int i = 1; i < 1000; i++)
                {
                    sw.WriteLine("Hello and Welcome" + Environment.NewLine);
                    Thread.Sleep(3);
                }


                MessageBox.Show("data saved into File!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();
            }
        }

        private void btn_bro_wrt_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txt_browse_write.Text = saveFileDialog1.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox_read.Items.Clear();
            listBox_write.Items.Clear();
            th = new Thread(ReadData);
            th1 = new Thread(WriteData);

            th.Start();
            th1.Start();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            th = new Thread(ReadDataForTaskTwo);
            th.Start();
            th4 = new Thread(WriteDataForTaskTwo);
            th4.Start();
                     
        }

        private void ReadDataForTaskTwo()
        {
            if (txt_browse_read.Text.Length < 3)
            {
                MessageBox.Show("No File Selected Yet !!!!");
                return;
            }
            listBox_read.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                fs = new FileStream(txt_browse_read.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listBox_read.Items.Add(line);                                        
                    th3 = new Thread(() => AddDataIntoList(line));
                    th3.Start();
                    Thread.Sleep(3);                    
                }

                MessageBox.Show("data read from File!!!");
                MessageBox.Show("data saved into list!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
            }
        }
        private void AddDataIntoList(string data)
        {
            string ans = data.Replace('a', 'A');
            listBox_write.Items.Add(ans);
            
        }

        private void WriteDataForTaskTwo()
        {
            if (txt_browse_write.Text.Length < 3)
            {
                MessageBox.Show("No File Selected yet !");
                return;
            }
            string fname = txt_browse_write.Text;
            if (!File.Exists(fname))
            {
                fname = fname + ".txt";
                File.Create(fname).Dispose();
                using (TextWriter tw = new StreamWriter(fname))
                {
                    tw.Close();
                }

            }
            string context = File.ReadAllText(txt_browse_read.Text);
            string ans = context.Replace('a', 'A');
            File.AppendAllText(fname, ans);
            MessageBox.Show("data saved into File!!!");
        }
    }
}
