﻿namespace FileHandlingWithThread
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_read = new System.Windows.Forms.ListBox();
            this.listBox_write = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_browse_read = new System.Windows.Forms.Button();
            this.txt_browse_read = new System.Windows.Forms.TextBox();
            this.btn_bro_wrt = new System.Windows.Forms.Button();
            this.txt_browse_write = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_read
            // 
            this.listBox_read.FormattingEnabled = true;
            this.listBox_read.Location = new System.Drawing.Point(49, 103);
            this.listBox_read.Name = "listBox_read";
            this.listBox_read.Size = new System.Drawing.Size(281, 316);
            this.listBox_read.TabIndex = 2;
            // 
            // listBox_write
            // 
            this.listBox_write.FormattingEnabled = true;
            this.listBox_write.Location = new System.Drawing.Point(458, 103);
            this.listBox_write.Name = "listBox_write";
            this.listBox_write.Size = new System.Drawing.Size(267, 316);
            this.listBox_write.TabIndex = 3;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_browse_read
            // 
            this.btn_browse_read.Location = new System.Drawing.Point(49, 49);
            this.btn_browse_read.Name = "btn_browse_read";
            this.btn_browse_read.Size = new System.Drawing.Size(97, 23);
            this.btn_browse_read.TabIndex = 4;
            this.btn_browse_read.Text = "Read File Path";
            this.btn_browse_read.UseVisualStyleBackColor = true;
            this.btn_browse_read.Click += new System.EventHandler(this.btn_browse_read_Click);
            // 
            // txt_browse_read
            // 
            this.txt_browse_read.Location = new System.Drawing.Point(191, 52);
            this.txt_browse_read.Name = "txt_browse_read";
            this.txt_browse_read.Size = new System.Drawing.Size(139, 20);
            this.txt_browse_read.TabIndex = 5;
            // 
            // btn_bro_wrt
            // 
            this.btn_bro_wrt.Location = new System.Drawing.Point(458, 49);
            this.btn_bro_wrt.Name = "btn_bro_wrt";
            this.btn_bro_wrt.Size = new System.Drawing.Size(97, 23);
            this.btn_bro_wrt.TabIndex = 6;
            this.btn_bro_wrt.Text = "Write File Path";
            this.btn_bro_wrt.UseVisualStyleBackColor = true;
            this.btn_bro_wrt.Click += new System.EventHandler(this.btn_bro_wrt_Click);
            // 
            // txt_browse_write
            // 
            this.txt_browse_write.Location = new System.Drawing.Point(586, 52);
            this.txt_browse_write.Name = "txt_browse_write";
            this.txt_browse_write.Size = new System.Drawing.Size(139, 20);
            this.txt_browse_write.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(355, 209);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Task1 Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(355, 279);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Task2 Start";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 644);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt_browse_write);
            this.Controls.Add(this.btn_bro_wrt);
            this.Controls.Add(this.txt_browse_read);
            this.Controls.Add(this.btn_browse_read);
            this.Controls.Add(this.listBox_write);
            this.Controls.Add(this.listBox_read);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBox_read;
        private System.Windows.Forms.ListBox listBox_write;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_browse_read;
        private System.Windows.Forms.TextBox txt_browse_read;
        private System.Windows.Forms.Button btn_bro_wrt;
        private System.Windows.Forms.TextBox txt_browse_write;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

