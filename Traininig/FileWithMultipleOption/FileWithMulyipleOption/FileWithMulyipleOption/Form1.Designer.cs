﻿namespace FileWithMulyipleOption
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_browse = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txt_folderpath = new System.Windows.Forms.TextBox();
            this.btn_showfile = new System.Windows.Forms.Button();
            this.btn_showfilecontain = new System.Windows.Forms.Button();
            this.listBox_showfilelist = new System.Windows.Forms.ListBox();
            this.listbox_showfiledata = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(145, 74);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 23);
            this.btn_browse.TabIndex = 0;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_folderpath
            // 
            this.txt_folderpath.Location = new System.Drawing.Point(322, 77);
            this.txt_folderpath.Name = "txt_folderpath";
            this.txt_folderpath.Size = new System.Drawing.Size(178, 20);
            this.txt_folderpath.TabIndex = 1;
            // 
            // btn_showfile
            // 
            this.btn_showfile.Location = new System.Drawing.Point(145, 149);
            this.btn_showfile.Name = "btn_showfile";
            this.btn_showfile.Size = new System.Drawing.Size(93, 23);
            this.btn_showfile.TabIndex = 2;
            this.btn_showfile.Text = "show file list";
            this.btn_showfile.UseVisualStyleBackColor = true;
            this.btn_showfile.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_showfilecontain
            // 
            this.btn_showfilecontain.Location = new System.Drawing.Point(425, 149);
            this.btn_showfilecontain.Name = "btn_showfilecontain";
            this.btn_showfilecontain.Size = new System.Drawing.Size(115, 23);
            this.btn_showfilecontain.TabIndex = 3;
            this.btn_showfilecontain.Text = "Show File Content";
            this.btn_showfilecontain.UseVisualStyleBackColor = true;
            this.btn_showfilecontain.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBox_showfilelist
            // 
            this.listBox_showfilelist.FormattingEnabled = true;
            this.listBox_showfilelist.Location = new System.Drawing.Point(25, 203);
            this.listBox_showfilelist.Name = "listBox_showfilelist";
            this.listBox_showfilelist.Size = new System.Drawing.Size(352, 277);
            this.listBox_showfilelist.TabIndex = 4;
            this.listBox_showfilelist.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // listbox_showfiledata
            // 
            this.listbox_showfiledata.FormattingEnabled = true;
            this.listbox_showfiledata.Location = new System.Drawing.Point(436, 203);
            this.listbox_showfiledata.Name = "listbox_showfiledata";
            this.listbox_showfiledata.Size = new System.Drawing.Size(375, 277);
            this.listbox_showfiledata.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 596);
            this.Controls.Add(this.listbox_showfiledata);
            this.Controls.Add(this.listBox_showfilelist);
            this.Controls.Add(this.btn_showfilecontain);
            this.Controls.Add(this.btn_showfile);
            this.Controls.Add(this.txt_folderpath);
            this.Controls.Add(this.btn_browse);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txt_folderpath;
        private System.Windows.Forms.Button btn_showfile;
        private System.Windows.Forms.Button btn_showfilecontain;
        private System.Windows.Forms.ListBox listBox_showfilelist;
        private System.Windows.Forms.ListBox listbox_showfiledata;
    }
}

