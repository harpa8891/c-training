﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileWithMulyipleOption
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string filename = string.Empty;
        private void button1_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog()==DialogResult.OK)
            {
                txt_folderpath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(!Directory.Exists(txt_folderpath.Text))
            {
                MessageBox.Show("Directory does not exsists!!!!");

            }
            else
            {
                string[] files = Directory.GetFiles(txt_folderpath.Text);
                foreach(string file in files)
                {
                    listBox_showfilelist.Items.Add(file);
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_showfilecontain.Enabled = true;
            //filename = sender.Text;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btn_showfilecontain.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listbox_showfiledata.Items.Clear();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                fs = new FileStream(listBox_showfilelist.SelectedItem.ToString(), FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs);

                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listbox_showfiledata.Items.Add(line);

                }

                MessageBox.Show("data read from File!!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
            }
        }
    }
}
